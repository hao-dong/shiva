﻿import { StyleDeclaration } from './StyleDeclaration';

export interface ImageConfig {
    path?: any;
    style?: StyleDeclaration;
}