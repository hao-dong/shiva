﻿import { Container } from './Container';

export class RootContainer extends Container {

    constructor() {
        super({
            root:true
        });
    }

}